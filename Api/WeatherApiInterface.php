<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Api;

/**
 * Interface for weather web api
 */
interface WeatherApiInterface
{
/**
* GET for test api
* @return boolean|array
*/
public function getLastWeather();
}
