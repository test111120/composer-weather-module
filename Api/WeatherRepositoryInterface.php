<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface for weather repository
 */
interface WeatherRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @return mixed
     */
    public function getAllItems();
}
