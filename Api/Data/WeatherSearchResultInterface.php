<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for search result
 */
interface WeatherSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return WeatherSearchResultInterface
     */
    public function setItems(array $items);
}
