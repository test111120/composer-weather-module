<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

/**
 * Weather interface for entity
 */
interface WeatherInterface extends ExtensibleDataInterface
{
    const ENTITY_ID = 'id';
}
