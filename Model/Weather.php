<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model;

use Magento\Framework\Model\AbstractModel;
use Weather\WeatherModule\Model\ResourceModel\WeatherResourse;
use Weather\WeatherModule\Api\Data\WeatherInterface;

/**
 * Weather model class
 */
class Weather extends AbstractModel implements WeatherInterface
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'weather_storage_model';

    /**
     * Initialize magento model.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(WeatherResourse::class);
    }
}
