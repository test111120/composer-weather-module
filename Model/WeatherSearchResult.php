<?php

/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model;

use Magento\Framework\Api\SearchResults;
use Weather\WeatherModule\Api\Data\WeatherSearchResultInterface;

/**
 * Search result class
 */
class WeatherSearchResult extends SearchResults implements WeatherSearchResultInterface
{

}
