<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model;


use Weather\WeatherModule\Model\WeatherFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Weather\WeatherModule\Api\WeatherRepositoryInterface;
use Weather\WeatherModule\Model\ResourceModel\WeatherResourse;
use Weather\WeatherModule\Model\ResourceModel\Weather\WeatherCollectionFactory;
use Weather\WeatherModule\Api\Data\WeatherInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Weather\WeatherModule\Api\Data\WeatherSearchResultInterfaceFactory;

/**
 * Repository for Weather items
 */
class WeatherRepository implements WeatherRepositoryInterface
{
    /**
     * @var \Weather\WeatherModule\Model\WeatherFactory
     */
    private $weatherFactory;

    /**
     * @var WeatherResourse
     */
    private $weatherResource;

    /**
     * @var WeatherSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;


    public function __construct(WeatherFactory                      $weatherFactory,
                                WeatherResourse                             $weatherResource,
                                WeatherCollectionFactory                $collectionFactory,
                                WeatherSearchResultInterfaceFactory $itemSearchResultInterfaceFactory,
                                CollectionProcessorInterface     $collectionProcessor)
    {
        $this->weatherFactory = $weatherFactory;
        $this->weatherResource = $weatherResource;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $itemSearchResultInterfaceFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param $id
     * @return mixed|Weather
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $item = $this->weatherFactory->create();
        $this->weatherResource->load($item, $id);
        if (!$item->getId()) {
            throw new NoSuchEntityException(__('Unable to find News with ID "%1"', $id));
        }
        return $item;
    }

    /**
     * @param $id
     * @return true
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $item = $this->getById($id);
        try{
            $this->weatherResource->delete($item);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not delete the entry: %1', $exception->getMessage())
            );
        }
        return true;
    }

    /**
     * @return mixed
     */
    public function getAllItems()
    {
        $items = $this->collectionFactory->create();
        return $items->getItems();
    }

    /**
     * @param WeatherInterface $item
     * @return true
     */
    public function save(WeatherInterface $item)
    {
        try{
            $this->weatherResource->save($item);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(
                __('Could not save: %1', $exception->getMessage())
            );
        }
        return true;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * @return mixed
     */
    public function getLastItem()
    {
        $collection = $this->collectionFactory->create();
        return $collection->getLastItem();
    }

}
