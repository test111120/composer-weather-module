<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model\Config\Source;

/**
 * Source model for available fields in weather config fields
 */
class WeatherFieldsList implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var array
     */
    protected $weather_fields;


    /**
     * @return array
     */
    public function toOptionArray()
    {
        if(empty($this->_options)) {
            $this->weather_fields[] =
                [
                    'value' => __('t_2m:C'),
                    'label' => __('Temperature in degrees Celsius')
                ];
            $this->weather_fields[] =
                [
                    'value' => __('wind_speed_10m:ms'),
                    'label' => __('Wind speed in ms')
                ];
            $this->weather_fields[] =
                [
                    'value' => __('precip_1h:mm'),
                    'label' => __('Precipitation accumulated over the past hour in millimeter')
                ];
            $this->weather_fields[] =
                [
                    'value' => __('precip_24h:mm'),
                    'label' => __('Precipitation accumulated over the past 24 hours in millimeter')
                ];
            $this->weather_fields[] =
                [
                    'value' => __('msl_pressure:hPa'),
                    'label' => __('Mean sea level pressure in hectopascal ')
                ];
        }
        return $this->weather_fields;
    }
}
