<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model\Api;

use Magento\Framework\Api\SearchCriteriaBuilder;
use Psr\Log\LoggerInterface;
use Weather\WeatherModule\Api\WeatherApiInterface;
use Weather\WeatherModule\Model\WeatherFactory;
use Weather\WeatherModule\Model\WeatherRepository;
use Weather\WeatherModule\Model\Api\MeteomaticsWeatherAPI;

/**
 * Weather web api class
 */
class Weather implements WeatherApiInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var WeatherRepository
     */
    protected $weatherRepository;

    /**
     * @var \Weather\WeatherModule\Model\Api\MeteomaticsWeatherAPI
     */
    protected $weatherAPI;

    /**
     * @var WeatherFactory
     */
    protected $weather;

    /**
     * @param LoggerInterface $logger
     * @param WeatherRepository $weatherRepository
     * @param \Weather\WeatherModule\Model\Api\MeteomaticsWeatherAPI $weatherAPI
     * @param WeatherFactory $weather
     */
    public function __construct(
        LoggerInterface       $logger,
        WeatherRepository $weatherRepository,
        MeteomaticsWeatherAPI $weatherAPI,
        WeatherFactory $weather,
    )
    {
        $this->logger = $logger;
        $this->weatherRepository = $weatherRepository;
        $this->weatherAPI = $weatherAPI;
        $this->weather = $weather;
    }

    /**
     * @return array|bool|string
     */
    public function getLastWeather()
    {
        $response = ['success' => false];
        try {
            $result = json_decode($this->weatherAPI->getResponse());
            if(empty($this->weatherRepository->getLastItem())) {

                $temperature = $result->data[0]->coordinates[0]->dates[0]->value;
                $windSpeed = $result->data[1]->coordinates[0]->dates[0]->value;
                $date = date("Y-m-d");
                $precip_1h = $result->data[2]->coordinates[0]->dates[0]->value;
                $precip_24h = $result->data[3]->coordinates[0]->dates[0]->value;
                $msl_pressure = $result->data[4]->coordinates[0]->dates[0]->value;

                $this->weather->create()
                    ->setDate($date)
                    ->setTemperature($temperature)
                    ->setWindSpeed($windSpeed)
                    ->setPrecip1h($precip_1h)
                    ->setPrecip24h($precip_24h)
                    ->setMslPressure($msl_pressure)
                    ->save();
            }

            $response = ['success' => true, 'message' => 'success', 'requiredFields' => $result->requiredFields,
                'WeatherObject' => $this->weatherRepository->getLastItem()->getData()];

        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
            $this->logger->info($e->getMessage());
        }
        return json_encode($response);
    }
}
