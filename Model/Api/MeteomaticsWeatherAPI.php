<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model\Api;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Encryption\EncryptorInterface;

/**
 * Weather web api class
 */
class MeteomaticsWeatherAPI
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * XML path to LAT
     */
    const XML_PATH_LAT = 'custom_weather/general/LAT';

    /**
     * XML path to LONG
     */
    const XML_PATH_LONG = 'custom_weather/general/LONG';

    /**
     * XML path to Login
     */
    const XML_PATH_LOGIN = 'custom_weather/general/login';

    /**
     * XML path to Password
     */
    const XML_PATH_PASSWORD = 'custom_weather/general/password';

    /**
     * XML path to Weather fields
     */
    const XML_PATH_WEATHER_FIELDS = 'custom_weather/general/weather_fields';

    /**
     * @param LoggerInterface $logger
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(LoggerInterface $logger, ScopeConfigInterface $scopeConfig, EncryptorInterface $encryptor)
    {
        $this->logger = $logger;
        $this->scopeConfig = $scopeConfig;
        $this->encryptor = $encryptor;
    }

    /**
     * @return false|string
     */
    public function getResponse()
    {
        $date = date("Y-m-d");
        $time = date("H:i:s");
        $LAT = $this->scopeConfig->getValue(self::XML_PATH_LAT);
        $LONG = $this->scopeConfig->getValue(self::XML_PATH_LONG);
        $login = $this->scopeConfig->getValue(self::XML_PATH_LOGIN);
        $password = $this->scopeConfig->getValue(self::XML_PATH_PASSWORD);
        $password = $this->encryptor->decrypt($password);

        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, 'https://api.meteomatics.com/'.$date.'T'.$time.
            'ZPT1H/t_2m:C,wind_speed_10m:ms,precip_1h:mm,precip_24h:mm,msl_pressure:hPa/'.$LAT.','.$LONG.'/json');
        curl_setopt($curl_handle, CURLOPT_USERPWD, $login.":".$password);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Meteomatics PHP connector (curl)');
        $response = curl_exec($curl_handle);
        $http_code = (int)curl_getinfo($curl_handle, CURLINFO_HTTP_CODE);
        curl_close($curl_handle);

        $requiredFields = $this->scopeConfig->getValue(self::XML_PATH_WEATHER_FIELDS);
        $response = json_decode($response);
        $response->requiredFields = $requiredFields;
        $response = json_encode($response);

        return $response;
    }

}
