<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model\ResourceModel\Weather;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Weather\WeatherModule\Model\ResourceModel\WeatherResourse;
use Weather\WeatherModule\Model\Weather;

/**
 * Weather collection class
 */
class WeatherCollection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'weather_storage_collection';

    /**
     * Initialize collection model.
     */
    protected function _construct()
    {
        $this->_init(Weather::class, WeatherResourse::class);
    }
}
