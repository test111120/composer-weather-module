<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Weather resourse model class
 */
class WeatherResourse extends AbstractDb
{
    /**
     * @var string
     */
    protected $_eventPrefix = 'weather_storage_resource_model';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('weather_storage', 'id');
        $this->_useIsObjectNew = true;
    }
}
