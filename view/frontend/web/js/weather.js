define([
    'uiComponent',
    'ko',
    'mage/storage',
    'jquery',
    'mage/translate',
], function (
    Component,
    ko,
    storage,
    $,
    $t
) {
    'use strict';

    return Component.extend({
        defaults: {
            messageResponse: ko.observable(''),
            temperature: ko.observable(''),
            date: ko.observable(''),
            windSpeed: ko.observable(''),
            precip1h: ko.observable(''),
            precip24h: ko.observable(''),
            mslPressure: ko.observable(''),
        },
        initialize(){
            this._super();
            storage.get(`rest/V1/weather`)
                .done(response => {
                    response = JSON.parse(response);

                    this.date(response.WeatherObject.date);
                    this.temperature(response.requiredFields.includes('t_2m:C') ? response.WeatherObject.temperature : '');
                    this.windSpeed(response.requiredFields.includes('wind_speed_10m:ms') ? response.WeatherObject.wind_speed : '');
                    this.precip1h(response.requiredFields.includes('precip_1h:mm') ? response.WeatherObject.precip_1h : '');
                    this.precip24h(response.requiredFields.includes('precip_24h:mm') ? response.WeatherObject.precip_24h : '');
                    this.mslPressure(response.requiredFields.includes('msl_pressure:hPa') ? response.WeatherObject.msl_pressure : '');

                    let tempMessage = this.temperature() !== '' ? " Temperature is " + this.temperature() + "&degC" + "<br>" : '';
                    let windMessage = this.windSpeed() !== '' ? " Wind speed is " + this.windSpeed() + " m/s" + "<br>" : '';
                    let precip1hMessage = this.precip1h() !== '' ? " Precip 1h is " + this.precip1h() + " mm" + "<br>" : '';
                    let precip24hMessage = this.precip24h() !== '' ? " Precip 24h is " + this.precip24h() + " mm" + "<br>" : '';
                    let mslMessage = this.mslPressure() !== '' ? " Msl pressure is " + this.mslPressure() + " hPa" + "<br>" : '';

                    let message = '';
                    message += "Weather for: " + this.date() + "<br>";
                    message += tempMessage;
                    message += windMessage;
                    message += precip1hMessage;
                    message += precip24hMessage;
                    message += mslMessage;

                    this.messageResponse(message);
                })
                .fail(() => {
                    this.messageResponse($t('Weather info was not found!'));
                })
        },
    });
})
