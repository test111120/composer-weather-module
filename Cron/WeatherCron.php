<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Cron;

use Weather\WeatherModule\Model\Api\MeteomaticsWeatherAPI;
use Psr\Log\LoggerInterface;
use Weather\WeatherModule\Model\WeatherFactory;

/**
 * Cron job to add weather info each hour in table weather_storage
 */
class WeatherCron
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var WeatherFactory
     */
    protected $weather;

    /**
     * @var MeteomaticsWeatherAPI
     */
    protected $weatherAPI;

    /**
     * @param LoggerInterface $logger
     * @param WeatherFactory $weather
     * @param MeteomaticsWeatherAPI $weatherAPI
     */
    public function __construct(LoggerInterface $logger, WeatherFactory $weather, MeteomaticsWeatherAPI $weatherAPI)
    {
        $this->logger = $logger;
        $this->weather = $weather;
        $this->weatherAPI = $weatherAPI;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $response = json_decode($this->weatherAPI->getResponse());

        $date = date("Y-m-d");
        $temperature = $response->data[0]->coordinates[0]->dates[0]->value;
        $windSpeed = $response->data[1]->coordinates[0]->dates[0]->value;
        $precip_1h = $response->data[2]->coordinates[0]->dates[0]->value;
        $precip_24h = $response->data[3]->coordinates[0]->dates[0]->value;
        $msl_pressure = $response->data[4]->coordinates[0]->dates[0]->value;

        $this->weather->create()
            ->setDate($date)
            ->setTemperature($temperature)
            ->setWindSpeed($windSpeed)
            ->setPrecip1h($precip_1h)
            ->setPrecip24h($precip_24h)
            ->setMslPressure($msl_pressure)
            ->save();
        $this->logger->info('Weather Cron Works');
    }
}
