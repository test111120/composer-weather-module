<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Cron;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface;
use Weather\WeatherModule\Model\ResourceModel\Weather\WeatherCollectionFactory;

/**
 * Cron job to clean weather info each day in table weather_storage
 */
class WeatherClean
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var WeatherCollectionFactory
     */
    protected $weatherCollection;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * XML path to Count
     */
    const XML_PATH_COUNT = 'custom_weather/general/count';

    public function __construct(LoggerInterface $logger,
                                WeatherCollectionFactory $weatherCollection,
                                ScopeConfigInterface $scopeConfig)
    {
        $this->logger = $logger;
        $this->weatherCollection = $weatherCollection;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return void
     */
    public function execute()
    {
        $countToLeave = $this->scopeConfig->getValue(self::XML_PATH_COUNT);
        $collection = $this->weatherCollection->create();
        $collectionSize = $collection->getSize();
        $index = 0;
        foreach ($collection as $item) {
            if(($collectionSize-$countToLeave)>$index){
                $item->delete();
                $index++;
            }
        }
        $this->logger->info('Weather Cleanup Works, was deleted: '.$index.' records');
    }
}
