<?php
/**
 * Copyright © Vasyl Samborskyi, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Weather\WeatherModule\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Weather\WeatherModule\Model\WeatherRepository;

/**
 * Controller Delete for weather grid
 */
class Delete extends \Magento\Backend\App\Action
{
    /**
     * @var WeatherRepository
     */
    protected $weatherRepository;

    /**
     * @param Context $context
     * @param WeatherRepository $weatherRepository
     */
    public function __construct(Context $context, WeatherRepository $weatherRepository)
    {
        parent::__construct($context);
        $this->weatherRepository= $weatherRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $this->weatherRepository->deleteById($id);
                $this->messageManager->addSuccess(__('Weather deleted'));
                return $resultRedirect->setPath('*/grid/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addError(__('Weather does not exist'));
        return $resultRedirect->setPath('*/grid/');
    }
}
