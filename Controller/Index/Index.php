<?php
namespace Weather\WeatherModule\Controller\Index;

//use Magento\Framework\Controller\ResultFactory;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;
use Weather\WeatherModule\Model\Api\MeteomaticsWeatherAPI;
use Magento\Framework\Encryption\EncryptorInterface;

class Index extends \Magento\Framework\App\Action\Action implements HttpGetActionInterface
{
    protected $_pageFactory;

    protected $weatherAPI;

    protected $scopeConfig;

    private $config;
    const XML_PATH_PASSWORD = 'custom_weather/general/password';

    public function __construct(
        Context      $context,
        PageFactory $pageFactory,
        MeteomaticsWeatherAPI $weatherAPI,
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $config,
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->weatherAPI = $weatherAPI;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
        $this->config = $config;
    }

    public function execute()
    {
        $response = $this->weatherAPI->getResponse();
        $test = $this->scopeConfig->getValue(self::XML_PATH_PASSWORD);
        $this->config->decrypt($test);

        /*$temperature = json_decode($response)->data[0]->coordinates[0]->dates[0]->value;
        $windSpeed = json_decode($response)->data[1]->coordinates[0]->dates[0]->value;
        $precip_1h = json_decode($response)->data[2]->coordinates[0]->dates[0]->value;
        $precip_24h = json_decode($response)->data[3]->coordinates[0]->dates[0]->value;
        $msl_pressure = json_decode($response)->data[4]->coordinates[0]->dates[0]->value;
        $date = date("Y-m-d");*/


        $page = $this->_pageFactory->create();
        $page->getConfig()->getTitle()->set(__("Weather"));
        return $page;
    }
}
